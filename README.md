# Convenient Things

convth is a Minecraft mod that is based on the [Fabric mod loader](https://fabricmc.net) and requires the [fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api) and [LibCD](https://www.curseforge.com/minecraft/mc-mods/libcd).

A (hopefully) up-to-date documentation of the mod can be found at [minecraft.cryx.li](http://minecraft.cryx.li/doku.php?id=plugins:convth:start).

## Contents

* Resource plants :: Grow your resources
  * Extended resource plants :: Grow rare vanilla resources, too
  * Mod support [Cotton Resources](https://www.curseforge.com/minecraft/mc-mods/cotton-resources)
  * Mod support [TechReborn](https://www.curseforge.com/minecraft/mc-mods/techreborn)
  * Mod support [Mechanized Steam Power](https://www.curseforge.com/minecraft/mc-mods/mechanized-steam-power)
* Farming tools
  * Growth crystals :: Boost grows speed of plants above
  * Sickles :: Harvest and replant at the same time
* Cobblestone Generator :: Slowly generates and stores cobblestone
* Lava Generator :: Turn stone into lava when placed over a heat source
* Hydorator :: Moisten or dry items depending on the block underneath
* Charms :: Gain effects just for keeping an item in your inventory
  * Hovering charm :: Somewhat flying
  * Repair charm :: Repair damaged items
  * Gift of the Khajiit :: Night vision
  * Salamander :: Fire protection
