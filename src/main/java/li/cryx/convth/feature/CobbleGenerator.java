package li.cryx.convth.feature;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import li.cryx.convth.ConvenientThingsMod;
import li.cryx.convth.block.CobblestoneGeneratorBlock;
import li.cryx.convth.entity.CobblestoneGeneratorEntity;
import li.cryx.convth.util.ModConfig;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

/**
 * This feature manages the cobblestone generator.
 *
 * @author cryxli
 */
public class CobbleGenerator extends AbstractFeature {

	private static final Logger LOG = LogManager.getFormatterLogger(ConvenientThingsMod.MOD_ID);

	/** Instance of the cobblestone generator */
	public static Block BLOCK_COBBLESTONE_GENERATOR = new CobblestoneGeneratorBlock();

	/** Item corresponding to the cobblestone generator */
	public static Item ITEM_COBBLESTONE_GENERATOR = new BlockItem(BLOCK_COBBLESTONE_GENERATOR,
			new Item.Settings().group(ConvenientThingsMod.ITEM_GROUP));

	/** Entity containing the logic of the cobblestone generator */
	public static BlockEntityType<CobblestoneGeneratorEntity> ENTITY_COBBLESTONE_GENERATOR;

	public CobbleGenerator(final ModConfig conf) {
		super(conf.isCobblestoneGeneratorEnabled());
	}

	@Override
	protected void initialise() {
		Registry.register(Registry.BLOCK, id("cobblestone_generator"), BLOCK_COBBLESTONE_GENERATOR);

		Registry.register(Registry.ITEM, id("cobblestone_generator"), ITEM_COBBLESTONE_GENERATOR);

		ENTITY_COBBLESTONE_GENERATOR = Registry.register(Registry.BLOCK_ENTITY, id("entity_cobblestone_generator"),
				BlockEntityType.Builder.create(CobblestoneGeneratorEntity::new, BLOCK_COBBLESTONE_GENERATOR)
						.build(null));

		LOG.info("Cobblestone generator registered");
	}

}
