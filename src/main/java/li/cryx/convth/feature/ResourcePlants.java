package li.cryx.convth.feature;

import li.cryx.convth.ConvenientThingsMod;
import li.cryx.convth.block.AbstractResourcePlant;
import li.cryx.convth.item.AbstractResourceSeed;
import li.cryx.convth.renderer.ResourcePlantColorProvider;
import li.cryx.convth.util.ModConfig;
import net.fabricmc.fabric.api.client.render.ColorProviderRegistry;
import net.fabricmc.fabric.impl.blockrenderlayer.BlockRenderLayerMapImpl;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.minecraft.block.Block;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Feature that manages the resource plants.
 *
 * @author cryxli
 */
public class ResourcePlants extends AbstractFeature {

    private static final Logger LOG = LogManager.getFormatterLogger(ConvenientThingsMod.MOD_ID);

    private static final Map<String, AbstractResourcePlant> PLANTS = new HashMap<>();

    private final boolean extendedEnabled;

    private ResourcePlants(boolean enabled, boolean extendedEnabled) {
        super(enabled);
        this.extendedEnabled = extendedEnabled;
    }

    public ResourcePlants() {
        this(true, false);
    }

    public ResourcePlants(final ModConfig conf) {
        this(conf.isResourcePlantsEnabled(), conf.isExtendedResourcePlantsEnabled());
    }

    @Override
    protected void initialise() {
        registerPlant("glowstone", Items.GLOWSTONE_DUST, 0xffbb55);
        registerPlant("redstone", Items.REDSTONE, 0xff3333);
        registerPlant("iron", Items.IRON_NUGGET, 0xdddddd);
        registerPlant("iron2", Items.IRON_INGOT, 0xdddddd);
        registerPlant("gold", Items.GOLD_NUGGET, 0xffff00);
        registerPlant("gold2", Items.GOLD_INGOT, 0xffff00);
        registerPlant("lapis", Items.LAPIS_LAZULI, 0x4433ff);
        registerPlant("quartz", Items.QUARTZ, 0xffeedd);

        if (extendedEnabled) {
            registerPlant("diamond", Items.DIAMOND, 0x49ead6);
            registerPlant("emerald", Items.EMERALD, 0x17da61);
            registerPlant("coal", Items.COAL, 0x555555);
        }

        initOptionalPlants();

        LOG.info("Resource plants registered");
    }

    @Override
    protected void initialiseClient() {
        final ResourcePlantColorProvider colorProvider = new ResourcePlantColorProvider();

        final Block[] plants = PLANTS.values().toArray(new Block[PLANTS.size()]);
        BlockRenderLayerMapImpl.INSTANCE.putBlocks(RenderLayer.getCutout(), plants);
        ColorProviderRegistry.BLOCK.register(colorProvider, plants);

        ArrayList<ItemConvertible> items = new ArrayList<>();
        for (AbstractResourcePlant plant : PLANTS.values()) {
            items.add(plant.getSeedsItem());
        }
        ColorProviderRegistry.ITEM.register(colorProvider, items.toArray(new Item[items.size()]));
    }

    private void initOptionalPlants() {
        // look for known mods that provide ores
        boolean cotton = false;
        boolean techReborn = false;
        boolean mechanized = false;
        for (ModContainer mod : FabricLoader.getInstance().getAllMods()) {
            final String modId = mod.getMetadata().getId();
            techReborn = techReborn | "techreborn".equals(modId);
            cotton = cotton | "cotton-resources".equals(modId);
            mechanized = mechanized | "mechanized".equals(modId);
            // TODO
        }

        // activate plants for TechReborn
        if (cotton) {
            LOG.info("Cotton resources are present");
            registerPlant("aluminum", new Identifier("c", "aluminum_ingot"), 0xf3cece);
        }
        if (techReborn) {
            LOG.info("TechReborn is present");
            registerPlant("nickel", new Identifier("techreborn", "nickel_ingot"), 0xb0ae8e);
        }
        if (mechanized) {
            LOG.info("Mechanized Steam Power is present");
            registerPlant("zinc", new Identifier("mechanized", "zinc_ingot"), 0xc7c7ca);
        }
        if (cotton || techReborn || mechanized) {
            registerPlant("techreborn_copper", new Identifier("c", "copper_ingot"), cotton, 0xe57b00, techReborn, 0xa16f56, mechanized, 0xd27e1f);
        }
        if (cotton || techReborn) {
            registerPlant("techreborn_lead", new Identifier("c", "lead_ingot"), cotton, 0x836d8f, techReborn, 0x5b585f);
            registerPlant("techreborn_silver", new Identifier("c", "silver_ingot"), cotton, 0xbacaf7, techReborn, 0xb1baba);
            registerPlant("techreborn_tin", new Identifier("c", "tin_ingot"), cotton, 0x85b8e3, techReborn, 0xc3c3c3);
        }
    }

    private void registerPlant(final String key, final AbstractResourcePlant plant) {
        if (!PLANTS.containsKey(key)) {
            Registry.register(Registry.BLOCK, id("plant_" + key), plant);
            Registry.register(Registry.ITEM, id("seed_" + key), new AbstractResourceSeed(plant));
            PLANTS.put(key, plant);
        }
    }

    private void registerPlant(final String key, final Identifier resourceItemId, int colorTint) {
        registerPlant(key, () -> Registry.ITEM.get(resourceItemId), colorTint);
    }

    private void registerPlant(final String key, final Identifier resourceItemId, Object... enabledTint) {
        for (int index = 0; index < enabledTint.length; index++) {
            if ((boolean) enabledTint[index++]) {
                registerPlant(key, () -> Registry.ITEM.get(resourceItemId), (int) enabledTint[index]);
            }
        }
    }

    private void registerPlant(final String key, final Supplier<Item> resourceItemSupplier, int colorTint) {
        registerPlant(key, new AbstractResourcePlant(resourceItemSupplier, colorTint));
    }

    private void registerPlant(final String key, Item resourceItem, int colorTint) {
        registerPlant(key, new AbstractResourcePlant(resourceItem, colorTint));
    }

}
