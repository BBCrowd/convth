package li.cryx.convth.feature;

import li.cryx.convth.item.Overkill;
import li.cryx.convth.util.ModConfig;
import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

public class WorkInProgress extends AbstractFeature {

    public static final Item SWORD_OVERKILL = new Overkill();

    public WorkInProgress(final ModConfig conf) {
        // TODO
        super(true);
    }

    @Override
    protected void initialise() {
        Registry.register(Registry.ITEM, id("overkill"), SWORD_OVERKILL);

//        final String itemName = "wither_proof";
//        Block block = new WitherProofObsidian(itemName);
//        Registry.register(Registry.BLOCK, id(itemName), block);
//        Registry.register(Registry.ITEM, id(itemName), new BlockItem(block,
//                new Item.Settings().group(ConvenientThingsMod.ITEM_GROUP)));
    }

}
