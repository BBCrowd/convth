package li.cryx.convth.feature;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;
import li.cryx.convth.ConvenientThingsMod;
import li.cryx.convth.block.HydoratorBlock;
import li.cryx.convth.entity.HydoratorEntity;
import li.cryx.convth.recipe.HydoratorRecipe;
import li.cryx.convth.renderer.HydoratorRenderer;
import li.cryx.convth.util.ModConfig;
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.InvalidIdentifierException;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This feature manages the hydorator including loading of recipes.
 *
 * @author cryxli
 */
public class HydoratorFeature extends AbstractFeature {

    private static final Logger LOG = LogManager.getFormatterLogger(ConvenientThingsMod.MOD_ID);

    /**
     * Instance of the cobblestone generator
     */
    public static HydoratorBlock BLOCK_HYDORATOR = new HydoratorBlock();

    /**
     * Item corresponding to the cobblestone generator
     */
    public static Item ITEM_HYDORATOR = new BlockItem(BLOCK_HYDORATOR, new Item.Settings().group(ConvenientThingsMod.ITEM_GROUP));

    public static BlockEntityType<HydoratorEntity> ENTITY_HYDORATOR;

    public HydoratorFeature() {
        super(true);
    }

    public HydoratorFeature(final ModConfig config) {
        super(config.isHydoratorEnabled());
    }

    @Override
    protected void initialise() {
        Registry.register(Registry.BLOCK, id("hydorator"), BLOCK_HYDORATOR);

        Registry.register(Registry.ITEM, id("hydorator"), ITEM_HYDORATOR);

        ENTITY_HYDORATOR = Registry.register(Registry.BLOCK_ENTITY, id("entity_hydorator"),
                BlockEntityType.Builder.create(HydoratorEntity::new, BLOCK_HYDORATOR).build(null));

        readRecipes();

        LOG.info("Hydorator registered");
    }

    private void readRecipes() {
        final File recipeFile = ModConfig.getConfigFile("hydorator-recipes.json");
        final Set<HydoratorRecipe> recipes = new LinkedHashSet<>();

        // defaults
        try (InputStream stream = getClass().getResourceAsStream("/assets/convth/hydorator-default.json")) {
            parseRecipe(recipes, stream);
        } catch (IOException e) {
            LOG.error("Default hydorator recipes have errors", e);
        }

        // load from config
        try (InputStream stream = new FileInputStream(recipeFile)) {
            parseRecipe(recipes, stream);
        } catch (FileNotFoundException e) {
            // do nothing
        } catch (IOException e) {
            LOG.error("Hydorator recipes have errors", e);
        }

        // register
        for (HydoratorRecipe recipe : recipes) {
            HydoratorEntity.addRecipe(recipe);
        }

        // save recipes
        writeRecipes(recipeFile);
        LOG.info(HydoratorEntity.getRecipes().size() + " hydorator recipes loaded");
    }

    private void writeRecipes(File recipeFile) {
        try (
                OutputStream stream = new FileOutputStream(recipeFile);
                Writer writer = new OutputStreamWriter(stream, StandardCharsets.UTF_8);
                JsonWriter out = new Gson().newJsonWriter(writer)
        ) {
            out.beginArray();
            for (HydoratorRecipe r : HydoratorEntity.getRecipes()) {
                out.beginObject();
                out.name("blockBelow").value(r.getBlockBelow().toString());
                out.name("inputStack").value(Registry.ITEM.getId(r.getInputStack().getItem()).toString());
                out.name("time").value(r.getTime());
                out.name("outputStack").value(Registry.ITEM.getId(r.getOutputStack().getItem()).toString());
                out.endObject();
            }
            out.endArray();
            out.flush();
        } catch (IOException e) {
            LOG.error("Error writing hydorator recipes", e);
        }
    }

    private void parseRecipe(Set<HydoratorRecipe> recipes, InputStream stream) throws IOException {
        final Type type = new TypeToken<ArrayList<Object>>() {
        }.getType();

        try (Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8)) {
            final List<?> entries = new Gson().fromJson(reader, type);
            if (entries == null) {
                return;
            }
            for (Object obj : entries) {
                if (!(obj instanceof Map)) {
                    continue;
                }
                final Map<String, Object> map = (Map<String, Object>) obj;
                try {
                    recipes.add(new HydoratorRecipe(
                            (String) map.get("blockBelow"),
                            (String) map.get("inputStack"),
                            ((Number) map.get("time")).intValue(),
                            (String) map.get("outputStack")
                    ));
                } catch (InvalidIdentifierException | ClassCastException e) {
                    LOG.warn("Hydorator recipe with error", e);
                }
            }
        }
    }

    @Override
    protected void initialiseClient() {
        BlockEntityRendererRegistry.INSTANCE.register(ENTITY_HYDORATOR, HydoratorRenderer::new);
    }

}
