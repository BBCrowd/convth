package li.cryx.convth.feature;

import li.cryx.convth.ConvenientThingsMod;
import net.minecraft.util.Identifier;

public abstract class AbstractFeature {

	private boolean enabled;

	protected AbstractFeature(final boolean enabled) {
		this.enabled = enabled;
	}

	protected Identifier id(final String key) {
		return new Identifier(ConvenientThingsMod.MOD_ID, key);
	}

	protected void initialise() {
	}

	protected void initialiseClient() {
	}

	public void onInit() {
		if (enabled) {
			initialise();
		}
	}

	public void onInitClient() {
		if (enabled) {
			initialiseClient();
		}
	}

}
