package li.cryx.convth.feature;

import li.cryx.convth.block.CharmSolidAirBlock;
import li.cryx.convth.charm.CharmManager;
import li.cryx.convth.entity.CharmSolidAirEntity;
import li.cryx.convth.item.FireResistCharm;
import li.cryx.convth.item.FlyingCharm;
import li.cryx.convth.item.KhajiitCharm;
import li.cryx.convth.item.RepairCharm;
import li.cryx.convth.util.ModConfig;
import net.fabricmc.fabric.api.event.server.ServerTickCallback;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.Item;
import net.minecraft.util.registry.Registry;

/**
 * This feature manages the charms. Charms are inspired by Diablo 2 where you
 * have items that would activate their effects for as long as they were kept in
 * the inventory of the player.
 *
 * @author cryxli
 */
public class Charms extends AbstractFeature {

    public static final Block SOLID_AIR = new CharmSolidAirBlock();
    private static final Item ITEM_REPAIR = new RepairCharm();
    private static final Item ITEM_FLYING = new FlyingCharm();
    private static final Item ITEM_FIRE_RES = new FireResistCharm();
    private static final Item ITEM_KHAJIIT = new KhajiitCharm();
    public static BlockEntityType<CharmSolidAirEntity> ENTITY_SOLID_AIR;

    public Charms(final ModConfig conf) {
        super(conf.isCharmsEnabled());
        ((KhajiitCharm) ITEM_KHAJIIT).setBlinkingEnabled(conf.isBlinkingEnabledOnKhajiitCharm());
    }

    @Override
    protected void initialise() {
        // register flying charm (experimental)
        Registry.register(Registry.BLOCK, id("solid_air"), SOLID_AIR);
        ENTITY_SOLID_AIR = Registry.register(Registry.BLOCK_ENTITY, id("entity_solid_air"),
                BlockEntityType.Builder.create(CharmSolidAirEntity::new, SOLID_AIR).build(null));
        Registry.register(Registry.ITEM, id("charm_flying"), ITEM_FLYING);

        // register charms
        Registry.register(Registry.ITEM, id("charm_repair"), ITEM_REPAIR);
        Registry.register(Registry.ITEM, id("charm_fire_res"), ITEM_FIRE_RES);
        Registry.register(Registry.ITEM, id("charm_khajiit"), ITEM_KHAJIIT);

        // handle server ticks on items, since they do not have a Tickable facility,
        // unlike Blocks
        ServerTickCallback.EVENT.register(new CharmManager());
    }

}
