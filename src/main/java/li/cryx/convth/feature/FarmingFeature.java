package li.cryx.convth.feature;

import li.cryx.convth.ConvenientThingsMod;
import li.cryx.convth.block.GrowthCrystal;
import li.cryx.convth.item.Sickle;
import li.cryx.convth.util.ModConfig;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ToolMaterials;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class FarmingFeature extends AbstractFeature {

    public FarmingFeature(ModConfig conf) {
        super(conf.isFarmingEnabled());
    }

    @Override
    protected void initialise() {
        // growth crystal
        final Block block = new GrowthCrystal();
        final Identifier id = id("growth_crystal");
        Registry.register(Registry.BLOCK, id, block);
        Registry.register(Registry.ITEM, id, new BlockItem(block, new Item.Settings().group(ConvenientThingsMod.ITEM_GROUP)));

        // sickles
        Registry.register(Registry.ITEM, id("wood_sickle"), new Sickle(ToolMaterials.WOOD));
        Registry.register(Registry.ITEM, id("stone_sickle"), new Sickle(ToolMaterials.STONE));
        Registry.register(Registry.ITEM, id("iron_sickle"), new Sickle(ToolMaterials.IRON));
        Registry.register(Registry.ITEM, id("gold_sickle"), new Sickle(ToolMaterials.GOLD));
        Registry.register(Registry.ITEM, id("diamond_sickle"), new Sickle(ToolMaterials.DIAMOND));

        // add more farming related tools
    }

}
