package li.cryx.convth.feature;

import li.cryx.convth.ConvenientThingsMod;
import li.cryx.convth.block.LavaGeneratorBlock;
import li.cryx.convth.entity.LavaGeneratorEntity;
import li.cryx.convth.util.ModConfig;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.InvalidIdentifierException;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * This feature manages the lava generator (crucible).
 *
 * @author cryxli
 */
public class LavaGenerator extends AbstractFeature {

    private static final Logger LOG = LogManager.getFormatterLogger(ConvenientThingsMod.MOD_ID);

    /**
     * Instance of the lava generator
     */
    public static Block BLOCK_CRUCIBLE = new LavaGeneratorBlock();

    /**
     * Item corresponding to the lava generator
     */
    public static Item ITEM_CRUCIBLE = new BlockItem(BLOCK_CRUCIBLE,
            new Item.Settings().group(ConvenientThingsMod.ITEM_GROUP));

    /**
     * Entity containing the logic of the lava generator
     */
    public static BlockEntityType<LavaGeneratorEntity> ENTITY_CRUCIBLE;

    public LavaGenerator() {
        super(true);
    }

    public LavaGenerator(final ModConfig config) {
        super(config.isLavaGeneratorEnabled());
    }

    @Override
    protected void initialise() {
        Registry.register(Registry.BLOCK, id("crucible"), BLOCK_CRUCIBLE);

        Registry.register(Registry.ITEM, id("crucible"), ITEM_CRUCIBLE);

        ENTITY_CRUCIBLE = Registry.register(Registry.BLOCK_ENTITY, id("entity_crucible"),
                BlockEntityType.Builder.create(LavaGeneratorEntity::new, BLOCK_CRUCIBLE).build(null));

        loadHeatSources();

        LOG.info("Lava generator registered");
    }

    private void loadHeatSources() {
        final File file = ModConfig.getConfigFile("heat-sources.properties");

        // defaults
        final Properties prop = new Properties();
        prop.setProperty("minecraft:magma", "2");
        prop.setProperty("minecraft:glowstone", "2");
        prop.setProperty("minecraft:torch", "1");
        prop.setProperty("minecraft:fire", "4");
        prop.setProperty("minecraft:flowing_lava", "3");
        prop.setProperty("minecraft:lava", "3");

        // current
        try (FileInputStream stream = new FileInputStream(file)) {
            prop.load(stream);
        } catch (IOException e) {
            LOG.debug("Error loading heat sources", e);
        }

        // apply
        for (Entry<Object, Object> e : prop.entrySet()) {
            try {
                LavaGeneratorEntity.addHeatSource((String) e.getKey(), Integer.parseInt((String) e.getValue()));
            } catch (NumberFormatException | NullPointerException | InvalidIdentifierException err) {
                LOG.warn("Heat for " + e.getKey() + " is not numeric.", e);
            }
        }

        // save
        try (FileOutputStream stream = new FileOutputStream(file)) {
            prop.store(stream, "Heat sources for lava generator.");
            stream.flush();
        } catch (IOException e) {
            LOG.warn("Failed to save heat sources.", e);
        }
    }

}
