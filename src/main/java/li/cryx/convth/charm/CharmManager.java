package li.cryx.convth.charm;

import li.cryx.convth.feature.Charms;
import net.fabricmc.fabric.api.event.server.ServerTickCallback;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;

/**
 * Handler for charms to delegate ticks to them.
 *
 * @author cryxli
 * @see Charms
 */
public class CharmManager implements ServerTickCallback {

    /**
     * How often this charm is activated / does its work. [world ticks]
     */
    public static final int UPDATE_INTERVAL = 100;

    /**
     * Tag name for tick count
     */
    public static final String TAG_UPDATE_TICK = "updateTick";

    /**
     * Get the tags of the given stack. Also create an initial state, if no tags are
     * present.
     *
     * @param itemStack An <code>ItemStack</code> that represents a
     *                  {@link CharmTickable} item.
     * @return Current tags of the stack.
     */
    public static CompoundTag checkTag(final ItemStack itemStack) {
        if (!itemStack.hasTag()) {
            itemStack.setTag(new CompoundTag());
        }
        if (!itemStack.getTag().contains(TAG_UPDATE_TICK)) {
            itemStack.getTag().putInt(TAG_UPDATE_TICK, 0);
        }
        return itemStack.getTag();
    }

    @Override
    public void tick(final MinecraftServer server) {
        // check all players...
        for (ServerPlayerEntity player : server.getPlayerManager().getPlayerList()) {
            // ...for inventory items that need to be ticked...
            for (int j = 0; j < player.inventory.getInvSize(); ++j) {
                final ItemStack itemStack = player.inventory.getInvStack(j);
                // ...and tick them
                if (itemStack.getItem() instanceof CharmTickable) {
                    // if threashold is reached
                    tick(player, itemStack);
                } else if (itemStack.getItem() instanceof CharmFullTickable) {
                    // every server tick
                    ((CharmFullTickable) itemStack.getItem()).tick(player, itemStack);
                }
            }
        }
    }

    /**
     * Execute the tick on the given stack and delegate "work" when threshold is
     * reached.
     *
     * @param player    The current player holding the stack.
     * @param itemStack A <code>ItemStack</code> that represents a
     *                  {@link CharmTickable} item.
     */
    private void tick(final ServerPlayerEntity player, final ItemStack itemStack) {
        final CompoundTag tag = checkTag(itemStack);
        // execute tick
        int updateTick = tag.getInt(TAG_UPDATE_TICK) + 1;
        if (updateTick >= UPDATE_INTERVAL) {
            // threshold is reached, activate effect of item
            updateTick = 0;
            ((CharmTickable) itemStack.getItem()).doWork(player, itemStack);
        }
        // update stack to keep track of update ticks.
        tag.putInt(TAG_UPDATE_TICK, updateTick);
    }

}
