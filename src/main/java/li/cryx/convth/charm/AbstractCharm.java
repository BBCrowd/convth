package li.cryx.convth.charm;

import li.cryx.convth.ConvenientThingsMod;
import li.cryx.convth.feature.Charms;
import net.minecraft.item.Item;
import net.minecraft.util.Rarity;

/**
 * Charms are special items that cannot be stacked and usually need to be
 * updated by/with server ticks.
 *
 * @author cryxli
 * @see CharmManager
 * @see Charms
 */
public class AbstractCharm extends Item {

	/** Create a new epic charm. */
	protected AbstractCharm() {
		this(Rarity.EPIC);
	}

	/**
	 * Create a new charm.
	 *
	 * @param rarity Rarity of the charm. The rarity affects the colour of the item
	 *               name in the inventory.
	 */
	protected AbstractCharm(final Rarity rarity) {
		super(new Settings().maxCount(1).group(ConvenientThingsMod.ITEM_GROUP).rarity(rarity));
	}

}
