package li.cryx.convth.charm;

import li.cryx.convth.feature.Charms;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;

/**
 * Interface implemented by <code>Item</code>s that would like to receive server
 * ticks. Only <code>Block</code>s have a built in ability to listen to server
 * ticks. Therefore, the {@link CharmManager} does delegate ticks to the item
 * implementing this interface.
 *
 * @author cryxli
 * @see Charms
 */
public interface CharmFullTickable {

	/**
	 * Tick the charm with every server tick.
	 */
	void tick(final ServerPlayerEntity player, final ItemStack itemStack);

}
