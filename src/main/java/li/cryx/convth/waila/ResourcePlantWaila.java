package li.cryx.convth.waila;

import li.cryx.convth.block.AbstractResourcePlant;
import mcp.mobius.waila.api.IComponentProvider;
import mcp.mobius.waila.api.IDataAccessor;
import mcp.mobius.waila.api.IPluginConfig;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;

/**
 * Provider to tell WAILA what icon to display on {@link AbstractResourcePlant}s.
 */
public class ResourcePlantWaila implements IComponentProvider {

    public static final ResourcePlantWaila INSTANCE = new ResourcePlantWaila();

    @Override
    public ItemStack getStack(IDataAccessor accessor, IPluginConfig config) {
        final Block block = accessor.getBlock();
        if (block instanceof AbstractResourcePlant) {
            return ((AbstractResourcePlant) block).getResourceItem();
        } else {
            return ItemStack.EMPTY;
        }
    }

}
