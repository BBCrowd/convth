package li.cryx.convth.waila;

import java.util.List;

import li.cryx.convth.entity.LavaGeneratorEntity;
import mcp.mobius.waila.api.IComponentProvider;
import mcp.mobius.waila.api.IDataAccessor;
import mcp.mobius.waila.api.IPluginConfig;
import mcp.mobius.waila.api.IServerDataProvider;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.world.World;

public class LavaGeneratorWaila implements IComponentProvider, IServerDataProvider<BlockEntity> {

	public static final LavaGeneratorWaila INSTANCE = new LavaGeneratorWaila();

	private LavaGeneratorWaila() {
		// singleton
	}

	// client side
	@Override
	public void appendHead(final List<Text> tooltip, final IDataAccessor accessor, final IPluginConfig config) {
		// display server info in HWYLA tooltip
		final CompoundTag tag = accessor.getServerData();
		final int mbLava = tag.getInt(LavaGeneratorEntity.TAG_LAVA);
		final int mbCobble = tag.getInt(LavaGeneratorEntity.TAG_COBBLE)
				+ ItemStack.fromTag(tag.getCompound(LavaGeneratorEntity.TAG_COBBLE_STACK)).getCount()
						* LavaGeneratorEntity.MB_COBBLE_PER_BLOCK;

		tooltip.add(new TranslatableText(Items.COBBLESTONE.getTranslationKey()).append(": ")
				.append(String.valueOf(mbCobble)).append(" mb"));
		tooltip.add(new TranslatableText(Blocks.LAVA.getTranslationKey()).append(": ").append(String.valueOf(mbLava))
				.append(" mb"));
	}

	// server side
	@Override
	public void appendServerData(final CompoundTag data, final ServerPlayerEntity player, final World world,
			final BlockEntity entity) {
		// send info about crucible to client
		entity.toTag(data);
	}

}
