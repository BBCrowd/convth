package li.cryx.convth.waila;

import li.cryx.convth.entity.HydoratorEntity;
import mcp.mobius.waila.api.IComponentProvider;
import mcp.mobius.waila.api.IDataAccessor;
import mcp.mobius.waila.api.IPluginConfig;
import mcp.mobius.waila.api.IServerDataProvider;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.world.World;

import java.util.List;

public class HydoratorWaila implements IComponentProvider, IServerDataProvider<BlockEntity> {

    public static final HydoratorWaila INSTANCE = new HydoratorWaila();

    private HydoratorWaila() {
        // singleton
    }

    @Override
    public void appendHead(List<Text> tooltip, IDataAccessor accessor, IPluginConfig config) {
        final CompoundTag tag = accessor.getServerData();
        final int ticks = tag.getInt(HydoratorEntity.TAG_TICK);
        if (ticks > 0) {
            final HydoratorEntity entity = (HydoratorEntity) accessor.getBlockEntity();
            if (entity.getRecipe() != null) {
                final int time = entity.getRecipe().getTime();
                final int progress = (int) (100.0 * ticks / time);
                tooltip.add(new TranslatableText("waila.convth.hydorator.tooltip", progress));
            }
        }
    }

    // server side
    @Override
    public void appendServerData(CompoundTag data, ServerPlayerEntity serverPlayerEntity, World world, BlockEntity entity) {
        // send info about crucible to client
        entity.toTag(data);
    }

}
