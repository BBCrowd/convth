package li.cryx.convth.waila;

import java.util.List;

import li.cryx.convth.entity.CobblestoneGeneratorEntity;
import mcp.mobius.waila.api.IComponentProvider;
import mcp.mobius.waila.api.IDataAccessor;
import mcp.mobius.waila.api.IPluginConfig;
import mcp.mobius.waila.api.IServerDataProvider;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.world.World;

public class CobblestoneGeneratorWaila implements IComponentProvider, IServerDataProvider<BlockEntity> {

	public static final CobblestoneGeneratorWaila INSTANCE = new CobblestoneGeneratorWaila();

	private CobblestoneGeneratorWaila() {
		// singleton
	}

	// client side
	@Override
	public void appendHead(final List<Text> tooltip, final IDataAccessor accessor, final IPluginConfig config) {
		final CompoundTag tag = accessor.getServerData();
		final int cobble = tag.getInt(CobblestoneGeneratorEntity.TAG_COBBLE);

		tooltip.add(new TranslatableText("waila.convth.cobblestone_generator.tooltip", cobble));
	}

	// server side
	@Override
	public void appendServerData(final CompoundTag data, final ServerPlayerEntity player, final World world,
			final BlockEntity entity) {
		entity.toTag(data);
	}

}
