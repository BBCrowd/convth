package li.cryx.convth.waila;

import li.cryx.convth.block.AbstractResourcePlant;
import li.cryx.convth.block.LavaGeneratorBlock;
import li.cryx.convth.entity.CobblestoneGeneratorEntity;
import li.cryx.convth.entity.HydoratorEntity;
import li.cryx.convth.entity.LavaGeneratorEntity;
import mcp.mobius.waila.api.IRegistrar;
import mcp.mobius.waila.api.IWailaPlugin;
import mcp.mobius.waila.api.TooltipPosition;

/**
 * Configure WAILA/HWYLA to display additional info about entities of this mod.
 *
 * @author cryxli
 */
public class WailaPlugin implements IWailaPlugin {

    @Override
    public void register(final IRegistrar registrar) {
        // crucible
        registrar.registerComponentProvider(LavaGeneratorWaila.INSTANCE, TooltipPosition.HEAD,
                LavaGeneratorEntity.class);
        registrar.registerBlockDataProvider(LavaGeneratorWaila.INSTANCE, LavaGeneratorBlock.class);

        // cobblestone generator
        registrar.registerComponentProvider(CobblestoneGeneratorWaila.INSTANCE, TooltipPosition.HEAD,
                CobblestoneGeneratorEntity.class);
        registrar.registerBlockDataProvider(CobblestoneGeneratorWaila.INSTANCE, CobblestoneGeneratorEntity.class);

        // hydorator
        registrar.registerComponentProvider(HydoratorWaila.INSTANCE, TooltipPosition.HEAD,
                HydoratorEntity.class);
        registrar.registerBlockDataProvider(HydoratorWaila.INSTANCE, HydoratorEntity.class);

        // resource plants
        registrar.registerStackProvider(ResourcePlantWaila.INSTANCE, AbstractResourcePlant.class);
    }

}
