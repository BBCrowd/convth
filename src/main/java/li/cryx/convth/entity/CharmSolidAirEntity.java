package li.cryx.convth.entity;

import li.cryx.convth.block.CharmSolidAirBlock;
import li.cryx.convth.feature.Charms;
import li.cryx.convth.item.FlyingCharm;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;

/**
 * This entity is part of the {@link FlyingCharm}. When no player is nearby, the
 * corresponding {@link CharmSolidAirBlock} is removed from the world.
 *
 * @author cryxli
 * @see Charms
 */
public class CharmSolidAirEntity extends BlockEntity implements Tickable {

	public CharmSolidAirEntity() {
		super(Charms.ENTITY_SOLID_AIR);
	}

	@Override
	public void tick() {
		final BlockPos pos = getPos().up();
		for (PlayerEntity player : world.getPlayers()) {
			BlockPos playerPos = player.getBlockPos();
			if (playerPos.equals(pos) //
					|| playerPos.equals(pos.north()) //
					|| playerPos.equals(pos.south()) //
					|| playerPos.equals(pos.west()) //
					|| playerPos.equals(pos.east()) //
			) {
				// player is close, do nothing
				return;
			}
		}
		// there is no player near the block, remove it / turn it back into thin air
		world.removeBlock(getPos(), true);
	}

}
