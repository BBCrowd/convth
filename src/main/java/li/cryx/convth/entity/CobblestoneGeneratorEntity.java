package li.cryx.convth.entity;

import li.cryx.convth.feature.CobbleGenerator;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.Direction;

public class CobblestoneGeneratorEntity extends BlockEntity implements SidedInventory, Tickable {

    public static final String TAG_COBBLE = "cobblestone";

    /**
     * Max blocks of cobble the generator does store internally.
     */
    private static final int MAX_STORED_COBBLE = 32;

    /**
     * How many blocks of coble the generator has stored internally.
     */
    private int storedCobble;

    /**
     * Keep track of ticks.
     */
    private int updateTick = 0;

    public CobblestoneGeneratorEntity() {
        super(CobbleGenerator.ENTITY_COBBLESTONE_GENERATOR);
    }

    @Override
    public boolean canExtractInvStack(final int slot, final ItemStack stack, final Direction dir) {
        // allow to extract cobblestone from all sides
        return stack.getItem() == Items.COBBLESTONE //
                && stack.getCount() <= storedCobble;
    }

    @Override
    public boolean canInsertInvStack(final int slot, final ItemStack stack, final Direction dir) {
        // do not allow to insert anything
        return false;
    }

    @Override
    public boolean canPlayerUseInv(final PlayerEntity player) {
        // unsure what this does
        return true;
    }

    @Override
    public void clear() {
        // probably not needed
        storedCobble = 0;
        markDirty();
    }

    @Override
    public void fromTag(final CompoundTag tag) {
        // deserialize the BlockEntity
        super.fromTag(tag);
        storedCobble = tag.getInt(TAG_COBBLE);
        updateTick = tag.getInt("update");
    }

    @Override
    public int[] getInvAvailableSlots(final Direction side) {
        // simulate an inventory with one slot
        return new int[]{0};
    }

    @Override
    public int getInvSize() {
        // simulate an inventory with one slot
        return 1;
    }

    @Override
    public ItemStack getInvStack(final int slot) {
        return new ItemStack(Blocks.COBBLESTONE, storedCobble);
    }

    @Override
    public boolean isInvEmpty() {
        return storedCobble == 0;
    }

    @Override
    public ItemStack removeInvStack(final int slot) {
        return takeInvStack(slot, MAX_STORED_COBBLE);
    }

    @Override
    public void setInvStack(final int slot, final ItemStack stack) {
        // not supported
    }

    @Override
    public ItemStack takeInvStack(final int slot, final int amount) {
        if (storedCobble > amount && amount > 0) {
            final ItemStack stack = getInvStack(slot);
            stack.setCount(amount);
            storedCobble -= amount;
            markDirty();
            return stack;
        } else if (storedCobble <= amount) {
            final ItemStack stack = getInvStack(slot);
            storedCobble = 0;
            markDirty();
            return stack;
        } else {
            return null;
        }
    }

    @Override
    public void tick() {
        if (storedCobble < MAX_STORED_COBBLE) {
            updateTick++;
            if (updateTick >= 20) {
                updateTick = 0;
                storedCobble++;
            }
            markDirty();
        }
    }

    @Override
    public CompoundTag toTag(final CompoundTag tag) {
        // serialize
        super.toTag(tag);
        tag.putInt(TAG_COBBLE, storedCobble);
        tag.putInt("update", updateTick);
        return tag;
    }

}
