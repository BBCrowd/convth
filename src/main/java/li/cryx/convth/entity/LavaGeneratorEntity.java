package li.cryx.convth.entity;

import li.cryx.convth.block.LavaGeneratorBlock;
import li.cryx.convth.feature.LavaGenerator;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import net.minecraft.util.Tickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;

import java.util.HashMap;
import java.util.Map;

public class LavaGeneratorEntity extends BlockEntity implements Tickable, Inventory {

    public static final String TAG_COBBLE = "cobble";

    public static final String TAG_LAVA = "laval";

    public static final String TAG_COBBLE_STACK = "cobbleStack";
    /**
     * Conversion rate of cobblestone block to millibuckets [mb].
     */
    public static final int MB_COBBLE_PER_BLOCK = 250;
    /**
     * Blocks that can be placed underneath the curcible to produce heat (= how fast
     * cobble is converted into lava).
     */
    private static final Map<Identifier, Integer> HEAT_SOURCES = new HashMap<>();
    /**
     * Max stored lava in millibuckets [mb]
     */
    private static final int MAX_LAVA = 4000;

    /**
     * How many millibuckets make a bucket [mb]
     */
    private static final int MB_PER_BUCKET = 1000;
    private ItemStack cobbleStack = new ItemStack(Items.COBBLESTONE, 0);
    private int mbLava = 0;
    private int mbCobble = 0;
    private int updateTick = 0;

    public LavaGeneratorEntity() {
        super(LavaGenerator.ENTITY_CRUCIBLE);
    }

    public static void addHeatSource(final Identifier id, final int heat) {
        HEAT_SOURCES.put(id, heat);
    }

    public static void addHeatSource(final String id, final int heat) {
        addHeatSource(new Identifier(id), heat);
    }

    public void addCobblestone(final int amount) {
        cobbleStack = new ItemStack(Items.COBBLESTONE, cobbleStack.getCount() + amount);
        world.playSound((PlayerEntity) null, pos, SoundEvents.BLOCK_STONE_PLACE, SoundCategory.BLOCKS, 0.8F, 1.0F);
        updateLevel();
        markDirty();
    }

    public boolean canAdd(final ItemStack stack) {
        return isValidInvStack(0, new ItemStack(stack.getItem(), 1));
    }

    @Override
    public boolean canPlayerUseInv(final PlayerEntity player) {
        return true;
    }

    public boolean canRetrieve(final ItemStack stack) {
        return stack.getItem() == Items.BUCKET && mbLava >= MB_PER_BUCKET;
    }

    @Override
    public void clear() {
    }

    private void doMelt() {
        // check heat source
        final BlockPos pos = getPos().down();
        final BlockState state = world.getBlockState(pos);
        final Identifier id = Registry.BLOCK.getId(state.getBlock());
        if (!HEAT_SOURCES.containsKey(id) || HEAT_SOURCES.get(id) == null || HEAT_SOURCES.get(id) <= 0) {
            return;
        }

        int heat = HEAT_SOURCES.get(id);
        if (mbLava + heat > MAX_LAVA) {
            heat = MAX_LAVA - mbLava;
        }
        if (heat > mbCobble) {
            if (!cobbleStack.isEmpty()) {
                mbCobble += MB_COBBLE_PER_BLOCK;
                cobbleStack.decrement(1);
            } else {
                heat = mbCobble;
            }
        }

        mbCobble -= heat;
        mbLava += heat;
        updateLevel();
    }

    @Override
    public void fromTag(final CompoundTag tag) {
        super.fromTag(tag);
        cobbleStack = ItemStack.fromTag(tag.getCompound(TAG_COBBLE_STACK));
        updateTick = tag.getInt("udpateTick");
        mbCobble = tag.getInt(TAG_COBBLE);
        mbLava = tag.getInt(TAG_LAVA);
    }

    @Override
    public int getInvMaxStackAmount() {
        return 3;
    }

    @Override
    public int getInvSize() {
        return 1;
    }

    @Override
    public ItemStack getInvStack(final int slot) {
        return cobbleStack;
    }

    @Override
    public boolean isInvEmpty() {
        return cobbleStack.isEmpty();
    }

    @Override
    public boolean isValidInvStack(final int slot, final ItemStack stack) {
        return (stack.getItem() == Items.COBBLESTONE || stack.getItem() == Items.NETHERRACK)
                && stack.getCount() + cobbleStack.getCount() <= getInvMaxStackAmount();
    }

    @Override
    public ItemStack removeInvStack(final int slot) {
        return ItemStack.EMPTY;
    }

    public ItemStack retrieve() {
        mbLava -= MB_PER_BUCKET;
        world.playSound((PlayerEntity) null, pos, SoundEvents.ITEM_BUCKET_FILL, SoundCategory.BLOCKS, 1.0F, 1.0F);
        updateLevel();
        markDirty();
        return new ItemStack(Items.LAVA_BUCKET);
    }

    @Override
    public void setInvStack(final int slot, final ItemStack stack) {
        cobbleStack = stack;
    }

    @Override
    public ItemStack takeInvStack(final int slot, final int amount) {
        return ItemStack.EMPTY;
    }

    @Override
    public void tick() {
        if (mbCobble == 0 && !cobbleStack.isEmpty()) {
            mbCobble += MB_COBBLE_PER_BLOCK;
            cobbleStack.decrement(1);
            markDirty();
            updateLevel();
        }

        if (mbCobble > 0 && mbLava < MAX_LAVA) {
            updateTick++;
            if (updateTick >= 20) {
                updateTick = 0;
                doMelt();
            }
            markDirty();
        }
    }

    @Override
    public CompoundTag toTag(final CompoundTag tag) {
        tag.put(TAG_COBBLE_STACK, cobbleStack.toTag(new CompoundTag()));
        tag.putInt("udpateTick", updateTick);
        tag.putInt(TAG_COBBLE, mbCobble);
        tag.putInt(TAG_LAVA, mbLava);
        return super.toTag(tag);
    }

    private void updateLevel() {
        final BlockState state = world.getBlockState(getPos());
        final LavaGeneratorBlock block = (LavaGeneratorBlock) state.getBlock();
        if (mbCobble == 0 && mbLava == 0 && cobbleStack.isEmpty()) {
            // completely empty
            block.setLevel(world, pos, state, 0);
        } else {
            final int cobble = cobbleStack.getCount() + (mbCobble > 0 ? 1 : 0);
            final int lava = mbLava / MB_PER_BUCKET;
            if (cobble > lava) {
                // more cobble than lava, therefore at least 1 cobble
                block.setLevel(world, pos, state, cobble);
            } else if (lava > 0) {
                // more lava than cobble, at least 1 bucket worth of it
                block.setLevel(world, pos, state, lava + 4);
            } else {
                // not a whole cobble, not a whole bucket
                block.setLevel(world, pos, state, 9);
            }
        }
    }

}
