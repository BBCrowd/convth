package li.cryx.convth.entity;

import li.cryx.convth.feature.HydoratorFeature;
import li.cryx.convth.recipe.HydoratorRecipe;
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import net.minecraft.util.Tickable;
import net.minecraft.util.registry.Registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The hydorator can store up to a single ItemStack usually only 1 item. Over time this item is
 * hydrated or dried.
 * <p>Implements BlockEntityClientSerializable to avoid having to sync the internal entity state
 * with the renderer.</p>
 *
 * @author cryxli
 */
public class HydoratorEntity extends BlockEntity implements BlockEntityClientSerializable, Tickable {

    public static final String TAG_TICK = "updateTick";

    private static final String TAG_ITEM = "item";

    /**
     * Fast look-up structure
     */
    private static final Map<Identifier, Map<Item, HydoratorRecipe>> RECIPES = new HashMap<>();

    private ItemStack itemStack = ItemStack.EMPTY;

    private int updateTick = 0;

    public HydoratorEntity() {
        super(HydoratorFeature.ENTITY_HYDORATOR);
    }

    public static List<HydoratorRecipe> getRecipes() {
        final List<HydoratorRecipe> recipes = new ArrayList<>();
        for (Map<Item, HydoratorRecipe> subRec : RECIPES.values()) {
            recipes.addAll(subRec.values());
        }
        return recipes;
    }

    public static void addRecipe(Identifier blockBelow, ItemStack inputStack, int time, ItemStack outputStack) {
        addRecipe(new HydoratorRecipe(blockBelow, inputStack, time, outputStack));
    }

    public static void addRecipe(HydoratorRecipe recipe) {
        if (!RECIPES.containsKey(recipe.getBlockBelow())) {
            RECIPES.put(recipe.getBlockBelow(), new HashMap<>());
        }
        RECIPES.get(recipe.getBlockBelow()).put(recipe.getInputStack().getItem(), recipe);
    }

    public int getProgress() {
        return updateTick;
    }

    @Override
    public CompoundTag toTag(CompoundTag tag) {
        tag.putInt(TAG_TICK, updateTick);
        tag.put(TAG_ITEM, itemStack.toTag(new CompoundTag()));
        return super.toTag(tag);
    }

    @Override
    public void fromTag(CompoundTag tag) {
        super.fromTag(tag);
        itemStack = ItemStack.fromTag(tag.getCompound(TAG_ITEM));
        updateTick = tag.getInt(TAG_TICK);
    }

    public boolean canRemove() {
        return !itemStack.isEmpty();
    }

    public boolean canInsert(ItemStack stack) {
        return itemStack.isEmpty();
    }

    public void addItem(ItemStack stackInHand) {
        itemStack = stackInHand.copy();
        itemStack.setCount(1);
        updateTick = 0;
        markDirty();
        sync();
    }

    public ItemStack removeItem() {
        final ItemStack stack = itemStack;
        itemStack = ItemStack.EMPTY;
        updateTick = 0;
        markDirty();
        sync();
        return stack;
    }

    public boolean hasItem() {
        return !itemStack.isEmpty();
    }

    public ItemStack getItem() {
        return itemStack;
    }

    @Override
    public void fromClientTag(CompoundTag compoundTag) {
        fromTag(compoundTag);
    }

    @Override
    public CompoundTag toClientTag(CompoundTag compoundTag) {
        return toTag(compoundTag);
    }

    @Override
    public void tick() {
        if (!world.isClient) {
            final HydoratorRecipe recipe = getRecipe();
            if (recipe != null) {
                updateTick++;
                if (updateTick >= recipe.getTime()) {
                    updateTick = 0;
                    itemStack = recipe.getOutputStack().copy();
                    sync();
                    world.playSound(null, pos, SoundEvents.ITEM_CHORUS_FRUIT_TELEPORT, SoundCategory.BLOCKS, 5.0F, 2.0F);
                }
                markDirty();
            }
        }
    }

    public HydoratorRecipe getRecipe() {
        final Identifier blockBelow = Registry.BLOCK.getId(world.getBlockState(getPos().down()).getBlock());
        if (!RECIPES.containsKey(blockBelow)) {
            return null;
        } else {
            return RECIPES.get(blockBelow).get(itemStack.getItem());
        }
    }

}
