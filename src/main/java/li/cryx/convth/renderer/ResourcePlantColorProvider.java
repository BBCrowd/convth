package li.cryx.convth.renderer;

import li.cryx.convth.block.AbstractResourcePlant;
import li.cryx.convth.item.AbstractResourceSeed;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.color.block.BlockColorProvider;
import net.minecraft.client.color.item.ItemColorProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockRenderView;

public class ResourcePlantColorProvider implements BlockColorProvider, ItemColorProvider {

    private static final int NO_TINT = -1;

    // BlockColorProvider
    @Override
    public int getColor(BlockState state, BlockRenderView view, BlockPos pos, int tintIndex) {
        final Block block = state.getBlock();
        if (block instanceof AbstractResourcePlant) {
            return ((AbstractResourcePlant) block).getColorTint();
        } else {
            return NO_TINT;
        }
    }

    // ItemColorProvider
    @Override
    public int getColor(ItemStack stack, int tintIndex) {
        if (tintIndex == 0 && (stack.getItem() instanceof AbstractResourceSeed)) {
            final AbstractResourceSeed seed = (AbstractResourceSeed) stack.getItem();
            final Block block = seed.getBlock();
            final AbstractResourcePlant plant = (AbstractResourcePlant) block;
            return plant.getColorTint();
        } else {
            return NO_TINT;
        }
    }

}
