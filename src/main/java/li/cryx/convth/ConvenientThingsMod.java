package li.cryx.convth;

import li.cryx.convth.feature.Charms;
import li.cryx.convth.feature.CobbleGenerator;
import li.cryx.convth.feature.FarmingFeature;
import li.cryx.convth.feature.HydoratorFeature;
import li.cryx.convth.feature.LavaGenerator;
import li.cryx.convth.feature.ResourcePlants;
import li.cryx.convth.feature.WorkInProgress;
import li.cryx.convth.util.ModConfig;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConvenientThingsMod implements ModInitializer {

    public static final String MOD_ID = "convth";

    private static final Logger LOG = LogManager.getFormatterLogger(MOD_ID);

    // Create a dummy item as the icon for the creative tab.
    private static final Item ITEM_GROUP_ICON = new Item(new Item.Settings());

    // Create a separate tab in the creative inventory for this mod.
    public static final ItemGroup ITEM_GROUP = FabricItemGroupBuilder.build( //
            new Identifier(MOD_ID, "general"), //
            () -> new ItemStack(ITEM_GROUP_ICON) //
    );

    @Override
    public void onInitialize() {
        Registry.register(Registry.ITEM, new Identifier(MOD_ID, "group_icon"), ITEM_GROUP_ICON);

        final ModConfig conf = new ModConfig().load();
        new ResourcePlants(conf).onInit();
        new CobbleGenerator(conf).onInit();
        new Charms(conf).onInit();
        new LavaGenerator(conf).onInit();
        new HydoratorFeature(conf).onInit();
        new FarmingFeature(conf).onInit();
        new WorkInProgress(conf).onInit();

        conf.save();
        LOG.info("Init done.");
    }

}
