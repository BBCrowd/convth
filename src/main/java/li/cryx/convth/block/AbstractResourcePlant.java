package li.cryx.convth.block;

import li.cryx.convth.feature.ResourcePlants;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.BlockState;
import net.minecraft.block.CropBlock;
import net.minecraft.block.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.TickPriority;
import net.minecraft.world.World;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

/**
 * A plant that grows resources.
 *
 * <p>
 * Growing mechanics is handled by the parent class <code>CropBlock</code>.
 * </p>
 *
 * <p>The harvested items and additional seeds are defined in the loot_tables for the plants. Look for <code>data.convth.loot_tables_blocks/plant_X.json</code> files.</p>
 *
 * @author cryxli
 * @see ResourcePlants
 */
public class AbstractResourcePlant extends CropBlock {

    // seconds
    private static final int AVERAGE_RANDOM_TICK = 34;

    /**
     * "Delayed" setter for {@link #resourceItem}.
     */
    private Supplier<Item> resourceItemSupplier;

    /**
     * The item this plant produces.
     */
    private ItemStack resourceItem;

    /**
     * The color used to tint the petals of the fully grown plant.
     */
    private int colorTint;

    private AbstractResourcePlant() {
        super(FabricBlockSettings.of(Material.PLANT).noCollision().ticksRandomly().breakInstantly()
                .sounds(BlockSoundGroup.CROP).build());
    }

    /**
     * Create a new resource plant.
     *
     * @param colorTint The color used to tint the petals of the fully grown plant.
     */
    public AbstractResourcePlant(int colorTint) {
        this();
        this.colorTint = colorTint;
    }

    /**
     * Create a new resource plant.
     *
     * @param resourceItem Produced item.
     * @param colorTint    The color used to tint the petals of the fully grown plant.
     */
    public AbstractResourcePlant(Item resourceItem, int colorTint) {
        this(colorTint);
        this.resourceItem = new ItemStack(resourceItem);
    }

    /**
     * Create a new resource plant.
     *
     * @param resourceItemSupplier Produced item.
     * @param colorTint            The color used to tint the petals of the fully grown plant.
     */
    public AbstractResourcePlant(Supplier<Item> resourceItemSupplier, int colorTint) {
        this(colorTint);
        this.resourceItemSupplier = resourceItemSupplier;
    }

    /**
     * Get the produced item.
     * <p>This method is used to tell WAILA which icon to display when looking at the plant.</p>
     *
     * @return Produced item, or, {@link ItemStack#EMPTY}.
     * @see li.cryx.convth.waila.ResourcePlantWaila
     */
    public ItemStack getResourceItem() {
        if (resourceItem == null) {
            if (resourceItemSupplier != null) {
                resourceItem = new ItemStack(resourceItemSupplier.get());
                resourceItemSupplier = null;
            } else {
                resourceItem = ItemStack.EMPTY;
            }
        }
        return resourceItem;
    }

    @Override
    public ItemConvertible getSeedsItem() {
        return asItem();
    }

    // not happy to override a deprecated method
    @Override
    public ActionResult onUse(final BlockState state, final World world, final BlockPos pos,
                              final PlayerEntity player,
                              final Hand hand, final BlockHitResult hit) {
        if (!world.isClient() && isMature(state)) {
            // harvest and replant
            final List<ItemStack> stacks = harvestAndReplant(state, world, pos);
            stacks.forEach((itemStack) -> {
                dropStack(world, pos, itemStack);
            });
            world.playSound(player, pos, SoundEvents.BLOCK_CROP_BREAK, SoundCategory.BLOCKS, 0.8F,
                    1.0F);

            return ActionResult.PASS;
        } else {
            // show some internals of the plant when hit with the sword
//            if (!world.isClient && player.getStackInHand(hand).getItem() == WorkInProcess.SWORD_OVERKILL) {
//                LogManager.getFormatterLogger(getClass()).warn(new StringBuffer() //
//                        .append(getTranslationKey()).append("(") //
//                        .append("pos=").append(pos).append(", ") //
//                        .append("age=").append(getAge(state)).append(", ") //
//                        .append("asItem()=").append(asItem()) //
//                        .append(")").toString() //
//                );
//            }
            return super.onUse(state, world, pos, player, hand, hit);
        }
    }

    public List<ItemStack> harvestAndReplant(BlockState state, final World world, final BlockPos pos) {
        // harvest without breaking
        final List<ItemStack> stacks = getDroppedStacks(state, (ServerWorld) world, pos, null);
        // by convention the first ItemStack is the seed that drops always
        stacks.remove(0);

        // reset plant
        world.setBlockState(pos, getDefaultState(), 2);

        return stacks;
    }

    @Override
    public void scheduledTick(final BlockState state, final ServerWorld world, final BlockPos pos,
                              final Random random) {
        super.scheduledTick(state, world, pos, random);
        if (50 > world.random.nextInt(100)) {
            world.getBlockTickScheduler().schedule(pos, this, 10 * AVERAGE_RANDOM_TICK, TickPriority.LOW);
        }
    }

    /**
     * Called by {@link li.cryx.convth.renderer.ResourcePlantColorProvider} to tint the petals of fully grown plants.
     */
    public int getColorTint() {
        return colorTint;
    }

}
