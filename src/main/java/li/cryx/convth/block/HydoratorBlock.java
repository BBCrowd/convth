package li.cryx.convth.block;

import li.cryx.convth.entity.HydoratorEntity;
import li.cryx.convth.feature.HydoratorFeature;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.EntityContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

/**
 * The hydorator is a machine block that dries or moisturises itmes depending on the fluid below.
 * When placed over water, it can turn cobble into mossy cobble, or turn wool white, etc.
 * When placed over lava, it can turn mossy cobble into regular cobble.
 *
 * @author cryxli
 */
public class HydoratorBlock extends Block implements BlockEntityProvider {

    private static final VoxelShape BOTTOM_SHAPE = Block.createCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 8.0D, 16.0D);

    public HydoratorBlock() {
        super(Settings.of(Material.STONE, MaterialColor.STONE).strength(2.0F, 6.0F));
    }

    @Override
    public BlockEntity createBlockEntity(final BlockView view) {
        return new HydoratorEntity();
    }

    @Override
    public boolean hasSidedTransparency(BlockState state) {
        return true;
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView view, BlockPos pos, EntityContext ePos) {
        return BOTTOM_SHAPE;
    }

    @Override
    public void afterBreak(final World world, final PlayerEntity player, final BlockPos pos, final BlockState state,
                           final BlockEntity blockEntity, final ItemStack stack) {
        if (!world.isClient) {
            dropStack(world, pos, new ItemStack(HydoratorFeature.ITEM_HYDORATOR, 1));
        } else {
            super.afterBreak(world, player, pos, state, blockEntity, stack);
        }
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        if (world.isClient) {
            return ActionResult.SUCCESS;
        } else {
            final BlockEntity blockEntity = world.getBlockEntity(pos);
            if (blockEntity != null && blockEntity instanceof HydoratorEntity) {
                final HydoratorEntity entity = (HydoratorEntity) blockEntity;
                final ItemStack stackInHand = player.getStackInHand(hand);
                if (stackInHand.isEmpty() && entity.canRemove()) {
                    // remove
                    player.inventory.offerOrDrop(world, entity.removeItem());
                    return ActionResult.SUCCESS;
                } else if (!stackInHand.isEmpty() && entity.canInsert(stackInHand)) {
                    // insert
                    entity.addItem(stackInHand);
                    stackInHand.decrement(1);
                    return ActionResult.SUCCESS;
                }
            }
        }
        return ActionResult.PASS;
    }

}
