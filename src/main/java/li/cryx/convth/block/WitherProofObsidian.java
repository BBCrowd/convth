package li.cryx.convth.block;

import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.block.MaterialColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class WitherProofObsidian extends Block {

    public WitherProofObsidian(String itemName) {
        super(FabricBlockSettings.of(Material.METAL, MaterialColor.BLACK)
                .strength(-1.0F, 3600000.0F)
                .sounds(BlockSoundGroup.ANVIL)
                .build()
        );
    }

    // make block break very easily by players
    @Override
    public void onBlockBreakStart(BlockState state, World world, BlockPos pos, PlayerEntity player) {
        if (!world.isClient) {
            dropStack(world, pos, new ItemStack(asItem()));
            world.setBlockState(pos, Blocks.AIR.getDefaultState());
        }
    }

//    @Override
//    public void onDestroyedByExplosion(World world, BlockPos pos, Explosion explosion) {
//        // TODO
//        if (!world.isClient) {
//            System.out.println("onDestroyedByExplosion " + pos);
//        }
//    }

//    @Override
//    public void onBroken(IWorld world, BlockPos pos, BlockState state) {
//        if (!world.isClient()) {
//            System.out.println("onBroken " + pos);
//        }
//    }

//    @Override
//    public void onProjectileHit(World world, BlockState state, BlockHitResult hitResult, Entity entity) {
//        // TODO remove
//        if (!world.isClient) {
//            System.out.println("onProjectileHit " + entity + " -> " + hitResult);
//        }
//    }

//    @Override
//    public boolean shouldDropItemsOnExplosion(Explosion explosion) {
//        return false;
//    }

}
