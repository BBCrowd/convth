package li.cryx.convth.block;

import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Fertilizable;
import net.minecraft.block.Material;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;

import java.util.Random;

public class GrowthCrystal extends Block {

    public GrowthCrystal() {
        super(FabricBlockSettings.of(Material.STONE)
                .strength(1, 1)
                .ticksRandomly()
                .breakByHand(true)
                .build()
        );
    }

    @Override
    public void scheduledTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        delegateGrowthTick(state, world, pos, random);
    }

    protected void delegateGrowthTick(BlockState state, ServerWorld world, BlockPos pos, Random random) {
        BlockPos upperPos = pos.up();
        BlockState upperState = world.getBlockState(upperPos);
        Block upperBlock = upperState.getBlock();
        if (upperBlock instanceof GrowthCrystal) {
            ((GrowthCrystal) upperBlock).delegateGrowthTick(upperState, world, upperPos, random);
            return;
        }

        upperPos = upperPos.up();
        upperState = world.getBlockState(upperPos);
        upperBlock = upperState.getBlock();
        if (upperBlock instanceof Fertilizable) {
            final Fertilizable f = (Fertilizable) upperBlock;
            if (f.canGrow(world, random, upperPos, upperState)) {
                f.grow(world, random, upperPos, upperState);
            }
        }
    }

}

