package li.cryx.convth.block;

import li.cryx.convth.entity.CharmSolidAirEntity;
import li.cryx.convth.feature.Charms;
import li.cryx.convth.item.FlyingCharm;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.TransparentBlock;
import net.minecraft.entity.EntityContext;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;

/**
 * This block is part of the {@link FlyingCharm}. It turns air a player is
 * standing on solid and removes itself once no player is nearby.
 *
 * @author cryxli
 * @see CharmSolidAirEntity
 * @see Charms
 */
public class CharmSolidAirBlock extends TransparentBlock implements BlockEntityProvider {

	public CharmSolidAirBlock() {
		super(FabricBlockSettings.of(Material.AIR).strength(-1.0F, 3600000.0F).dropsNothing().nonOpaque().build());
	}

	@Override
	public CharmSolidAirEntity createBlockEntity(final BlockView view) {
		return new CharmSolidAirEntity();
	}

	@Override
	public float getAmbientOcclusionLightLevel(final BlockState state, final BlockView view, final BlockPos pos) {
		return 1.0F;
	}

	@Override
	public VoxelShape getCollisionShape(final BlockState state, final BlockView view, final BlockPos pos,
			final EntityContext context) {
		// if entity above is descending (sneaking), make the block not having a
		// collision box
		if (context.isDescending()) {
			return VoxelShapes.empty();
		} else {
			// FIXME does allow fences/glass panes to connect
			return VoxelShapes.fullCube();
		}
	}

	@Override
	public VoxelShape getOutlineShape(final BlockState blockState, final BlockView blockView, final BlockPos blockPos,
			final EntityContext entityContext) {
		return VoxelShapes.empty();
	}

	@Override
	public BlockRenderType getRenderType(final BlockState state) {
		return BlockRenderType.INVISIBLE;
	}

	@Override
	public boolean isSideInvisible(final BlockState state, final BlockState neighbor, final Direction facing) {
		return true;
	}

	@Override
	public boolean isTranslucent(final BlockState state, final BlockView view, final BlockPos pos) {
		return true;
	}

}
