package li.cryx.convth.block;

import java.util.List;

import li.cryx.convth.entity.CobblestoneGeneratorEntity;
import li.cryx.convth.feature.CobbleGenerator;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

/**
 * Block that generates a cobblestone every second and stores up to 32 of them.
 * Players can activate the block to retrieve the generated cobblestone.
 * Supports hoppers for extracting, too.
 *
 * @author cryxli
 * @see CobbleGenerator
 */
public class CobblestoneGeneratorBlock extends Block implements BlockEntityProvider {

	public CobblestoneGeneratorBlock() {
		super(FabricBlockSettings.of(Material.STONE).strength(2.0F, 6.0F).build());
	}

	@Override
	public void afterBreak(final World world, final PlayerEntity player, final BlockPos pos, final BlockState state,
			final BlockEntity blockEntity, final ItemStack stack) {
		// when breaking the generator drop its stored cobble, too
		super.afterBreak(world, player, pos, state, blockEntity, stack);
		if (!world.isClient()) {
			dropStack(world, pos, new ItemStack(CobbleGenerator.ITEM_COBBLESTONE_GENERATOR));
			final CobblestoneGeneratorEntity entity = (CobblestoneGeneratorEntity) blockEntity;
			final ItemStack cobbleStack = entity.removeInvStack(0);
			if (cobbleStack != null) {
				dropStack(world, pos, cobbleStack);
			}
		}
	}

	@Override
	public void buildTooltip(final ItemStack stack, final BlockView view, final List<Text> tooltip,
			final TooltipContext options) {
		tooltip.add(new TranslatableText(getTranslationKey() + ".tooltip1")
				.setStyle(new Style().setColor(Formatting.GRAY)));
		tooltip.add(new TranslatableText(getTranslationKey() + ".tooltip2")
				.setStyle(new Style().setColor(Formatting.GRAY)));
	}

	@Override
	public CobblestoneGeneratorEntity createBlockEntity(final BlockView blockView) {
		return new CobblestoneGeneratorEntity();
	}

	@Override
	public ActionResult onUse(final BlockState state, final World world, final BlockPos pos, final PlayerEntity player,
			final Hand hand, final BlockHitResult hit) {
		if (world.isClient) {
			return ActionResult.SUCCESS;
		} else {
			final BlockEntity entity = world.getBlockEntity(pos);
			if (entity != null && entity instanceof CobblestoneGeneratorEntity) {
				final ItemStack stack = ((CobblestoneGeneratorEntity) entity).removeInvStack(0);
				if (stack != null) {
					player.inventory.offerOrDrop(world, stack);
					world.playSound((PlayerEntity) null, pos, SoundEvents.BLOCK_STONE_BREAK, SoundCategory.BLOCKS, 0.8F,
							1.0F);
					return ActionResult.SUCCESS;
				}
			}
		}
		return ActionResult.PASS;
	}

}
