package li.cryx.convth.block;

import java.util.List;

import li.cryx.convth.entity.LavaGeneratorEntity;
import li.cryx.convth.feature.LavaGenerator;
import net.fabricmc.fabric.api.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.EntityContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.IntProperty;
import net.minecraft.text.Style;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class LavaGeneratorBlock extends Block implements BlockEntityProvider {

	private static final VoxelShape SINGLE_SHAPE = Block.createCuboidShape(1.0D, 0.0D, 1.0D, 15.0D, 15.0D, 15.0D);

	/**
	 * Determines which model to display
	 * <ul>
	 * <li>0 = empty</li>
	 * <li>1 = 1 block of cobble, less than 1 bucket of lava</li>
	 * <li>2 = 2 blocks of cobble, less than 2 buckets of lava</li>
	 * <li>3 = 3 blocks of cobble, less than 3 buckets of lava</li>
	 * <li>4 = 4 blocks of cobble, less than 4 buckets of lava</li>
	 * <li>5 = 1 bucket of lava, less than 2 block of cobble</li>
	 * <li>6 = 2 buckets of lava, less than 3 blocks of cobble</li>
	 * <li>7 = 3 buckets of lava, less than 4 blocks of cobble</li>
	 * <li>8 = 4 buckets of lava</li>
	 * <li>9 = less than 1 block of cobble, less than 1 bucket of lava</li>
	 * </ul>
	 */
	public static final IntProperty LEVEL = IntProperty.of("level", 0, 9);

	public LavaGeneratorBlock() {
		super(FabricBlockSettings.of(Material.ANVIL).strength(2.0F, 6.0F).build());
		setDefaultState(stateManager.getDefaultState().with(LEVEL, 0));
	}

	@Override
	public void afterBreak(final World world, final PlayerEntity player, final BlockPos pos, final BlockState state,
			final BlockEntity blockEntity, final ItemStack stack) {
		if (!world.isClient) {
			dropStack(world, pos, new ItemStack(LavaGenerator.ITEM_CRUCIBLE, 1));
		} else {
			super.afterBreak(world, player, pos, state, blockEntity, stack);
		}
	}

	@Override
	protected void appendProperties(final StateManager.Builder<Block, BlockState> stateManager) {
		stateManager.add(LEVEL);
	}

	@Override
	public void buildTooltip(final ItemStack stack, final BlockView view, final List<Text> tooltip,
			final TooltipContext options) {
		tooltip.add(new TranslatableText(getTranslationKey() + ".tooltip1")
				.setStyle(new Style().setColor(Formatting.GRAY)));
		tooltip.add(new TranslatableText(getTranslationKey() + ".tooltip2")
				.setStyle(new Style().setColor(Formatting.GRAY)));
	}

	@Override
	public LavaGeneratorEntity createBlockEntity(final BlockView blockView) {
		return new LavaGeneratorEntity();
	}

	@Override
	public VoxelShape getOutlineShape(final BlockState state, final BlockView view, final BlockPos pos,
			final EntityContext ePos) {
		return SINGLE_SHAPE;
	}

	@Override
	public ActionResult onUse(final BlockState state, final World world, final BlockPos pos, final PlayerEntity player,
			final Hand hand, final BlockHitResult hit) {
		if (world.isClient) {
			// prevents client from placing cobblestone first, before it is inserted into
			// curcible
			return ActionResult.SUCCESS;
		} else if (hand == Hand.MAIN_HAND) {
			final BlockEntity blockEntity = world.getBlockEntity(pos);
			if (blockEntity != null && blockEntity instanceof LavaGeneratorEntity) {
				final LavaGeneratorEntity entity = (LavaGeneratorEntity) blockEntity;
				final ItemStack stackInHand = player.getStackInHand(hand);

				if (entity.canAdd(stackInHand)) {
					// add cobblestone or nether rack
					entity.addCobblestone(1);
					stackInHand.decrement(1);
					return ActionResult.SUCCESS;

				} else if (entity.canRetrieve(stackInHand)) {
					// retrieve lava
					stackInHand.decrement(1);
					player.inventory.offerOrDrop(world, entity.retrieve());
					return ActionResult.SUCCESS;
				}
			}
		}
		return ActionResult.PASS;
	}

	public void setLevel(final World world, final BlockPos pos, final BlockState state, final int level) {
		world.setBlockState(pos, state.with(LEVEL, MathHelper.clamp(level, 0, 9)), 2);
	}

}
