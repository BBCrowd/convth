package li.cryx.convth.util;

import net.minecraft.text.BaseText;
import net.minecraft.text.Style;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

/**
 * Since deobfuscation changes a lot, wrap in-game texts, so there is only one place that needs updating.
 */
public class TextUtil {

    private TextUtil() {
        // static singleton
    }

    public static TranslatableText translate(String key, Object... arguments) {
        return new TranslatableText(key, arguments);
    }

    public static <T extends BaseText> T withColor(T text, Formatting color) {
        text.setStyle(new Style().setColor(color));
        return text;
    }

    public static TranslatableText translate(Formatting color, String key, Object... arguments) {
        return withColor(translate(key, arguments), color);
    }

}
