package li.cryx.convth.util;

import li.cryx.convth.ConvenientThingsMod;
import net.fabricmc.loader.api.FabricLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class ModConfig extends Properties {

    public static final Logger LOG = LogManager.getFormatterLogger(ConvenientThingsMod.MOD_ID);

    private static final long serialVersionUID = -7921587166278256669L;

    private static final String ENABLE_PLANTS = "enable-resource-plants";

    private static final String ENABLE_COBBLESTONE_GENERATOR = "enable-cobblestone-generator";

    private static final String ENABLE_CHARMS = "enable-charms";

    private static final String ENABLE_LAVA_GENERATOR = "enable-lava-generator";

    private static final String ENABLE_HYDORATOR = "enable-hydorator";

    private static final String ENABLE_PLANTS_EXTENDED = "enable-extended-resource-plants";

    private static final String ENABLE_KHAJIIT_BLINKING = "enable-khajiit-blinking";

    private static final String ENABLE_FARMING_TOOLS = "enable-farming-tools";

    public ModConfig() {
        // defaults
        setProperty(ENABLE_PLANTS, "true");
        setProperty(ENABLE_COBBLESTONE_GENERATOR, "true");
        setProperty(ENABLE_CHARMS, "true");
        setProperty(ENABLE_LAVA_GENERATOR, "true");
        setProperty(ENABLE_HYDORATOR, "true");
        setProperty(ENABLE_PLANTS_EXTENDED, "false");
        setProperty(ENABLE_KHAJIIT_BLINKING, "true");
        setProperty(ENABLE_FARMING_TOOLS, "true");
    }

    public static File getConfigDirectory() {
        final File dir = new File(FabricLoader.getInstance().getConfigDirectory(), ConvenientThingsMod.MOD_ID);
        dir.mkdirs();
        return dir;
    }

    public static File getConfigFile(String filename) {
        return new File(getConfigDirectory(), filename);
    }

    private File getConfigFile() {
        return getConfigFile("config.properties");
    }

    /**
     * Indicator that the blinking of the background while under the effect of the {@link li.cryx.convth.item.KhajiitCharm} should be enabled.
     */
    public boolean isBlinkingEnabledOnKhajiitCharm() {
        return toBool(ENABLE_KHAJIIT_BLINKING);
    }

    /**
     * Indicator whether charms should be activated or not.
     */
    public boolean isCharmsEnabled() {
        return toBool(ENABLE_CHARMS);
    }

    /**
     * Indicator whether the cobblestone generator should be activated or not.
     */
    public boolean isCobblestoneGeneratorEnabled() {
        return toBool(ENABLE_COBBLESTONE_GENERATOR);
    }

    /**
     * Indicator whether the lava generator should be activated or not.
     */
    public boolean isLavaGeneratorEnabled() {
        return toBool(ENABLE_LAVA_GENERATOR);
    }

    /**
     * Indicator whether the hydorator should be activated or not.
     */
    public boolean isHydoratorEnabled() {
        return toBool(ENABLE_HYDORATOR);
    }

    /**
     * Indicator whether the resource plants should be activated or not.
     */
    public boolean isResourcePlantsEnabled() {
        return toBool(ENABLE_PLANTS);
    }

    /**
     * Indicator whether additional farming tools are activated or not.
     */
    public boolean isFarmingEnabled() {
        return toBool(ENABLE_FARMING_TOOLS);
    }

    public ModConfig load() {
        try (InputStream stream = new FileInputStream(getConfigFile())) {
            load(stream);
        } catch (IOException e) {
            LOG.warn("Cannot load config file, using defaults.");
        }
        return this;
    }

    public void save() {
        try (OutputStream stream = new FileOutputStream(getConfigFile())) {
            store(stream, "Configuration file for Convenient Things");
        } catch (IOException e) {
            LOG.error("Cannot save config file.", e);
        }
    }

    private boolean toBool(final String key) {
        final String s = getProperty(key);
        return "1".equals(s) || "true".equalsIgnoreCase(s);
    }

    public boolean isExtendedResourcePlantsEnabled() {
        return toBool(ENABLE_PLANTS_EXTENDED);
    }

}
