package li.cryx.convth.item;

import li.cryx.convth.ConvenientThingsMod;
import li.cryx.convth.util.TextUtil;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.item.ToolMaterials;
import net.minecraft.recipe.Ingredient;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

import java.util.List;

public class Overkill extends SwordItem {

    public Overkill() {
        super(new ToolMaterial() {
            @Override
            public float getAttackDamage() {
                return 500;
            }

            @Override
            public int getDurability() {
                return ToolMaterials.DIAMOND.getDurability();
            }

            @Override
            public int getEnchantability() {
                return ToolMaterials.DIAMOND.getEnchantability();
            }

            @Override
            public int getMiningLevel() {
                return ToolMaterials.DIAMOND.getMiningLevel();
            }

            @Override
            public float getMiningSpeed() {
                return ToolMaterials.DIAMOND.getMiningSpeed();
            }

            @Override
            public Ingredient getRepairIngredient() {
                return ToolMaterials.DIAMOND.getRepairIngredient();
            }
        }, 500, -2.4F, new Settings().group(ConvenientThingsMod.ITEM_GROUP));
    }

    @Override
    public void appendTooltip(final ItemStack stack, final World world, final List<Text> tooltip,
                              final TooltipContext context) {
        tooltip.add(TextUtil.translate(Formatting.DARK_GRAY, "item.convth.overkill.tooltip1"));
    }

}
