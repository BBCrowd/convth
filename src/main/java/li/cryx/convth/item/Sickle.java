package li.cryx.convth.item;

import li.cryx.convth.ConvenientThingsMod;
import li.cryx.convth.block.AbstractResourcePlant;
import li.cryx.convth.feature.Charms;
import li.cryx.convth.util.TextUtil;
import net.minecraft.block.AttachedStemBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.CactusBlock;
import net.minecraft.block.CropBlock;
import net.minecraft.block.GourdBlock;
import net.minecraft.block.NetherWartBlock;
import net.minecraft.block.PlantBlock;
import net.minecraft.block.SaplingBlock;
import net.minecraft.block.StemBlock;
import net.minecraft.block.SugarCaneBlock;
import net.minecraft.block.SweetBerryBushBlock;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.item.Items;
import net.minecraft.item.ToolItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.item.ToolMaterials;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Arrays;
import java.util.List;

/**
 * A sickle allows to harvest and replant plants in one go. Depending on the tool quality, a larger radios is harvested at once. Most vanilla plants ar supported.
 *
 * @since 0.8.0
 */
public class Sickle extends ToolItem {

    public Sickle(ToolMaterial material) {
        super(material, new Settings().maxCount(2).group(ConvenientThingsMod.ITEM_GROUP));
    }

    @Override
    public void appendTooltip(final ItemStack stack, final World world, final List<Text> tooltip,
                              final TooltipContext context) {
        tooltip.add(TextUtil.translate(Formatting.DARK_GRAY, getTranslationKey() + ".tooltip"));
    }

    /**
     * The material of the tool defines its range. Every harvested plant will damage the sickle for one point.
     *
     * <p>Example: A diamond sickle has a range of 3. The makes it harvest a 7x7 area.</p>
     */
    public int getRange() {
        ToolMaterial material = getMaterial();
        if (material == ToolMaterials.DIAMOND) {
            return 3;
        } else if (material == ToolMaterials.GOLD) {
            return 2;
        } else if (material == ToolMaterials.IRON) {
            return 1;
        } else {
            // wood and stone
            return 0;
        }
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        // only implemented server-side
        final World world = context.getWorld();
        if (world.isClient) {
            return ActionResult.PASS;
        }
        // the sickle takes damage when used
        final ItemStack sickle = context.getStack();
        // drop harvest at player's feet
        final BlockPos playerPos = context.getPlayer().getBlockPos();
        // the block hit by the sickle
        final BlockPos pos = context.getBlockPos();

        // depending on the range inspect plants
        int range = getRange();
        for (int x = -range; x <= range; x++) {
            for (int z = -range; z <= range; z++) {
                for (int y = 0; y <= 1; y++) {
                    // check and harvest, if mature
                    final BlockPos plantPos = pos.add(x, y, z);
                    harvestIfPlant(world, playerPos, plantPos, sickle);
                }
            }
        }
        checkToolBroken(sickle, context.getPlayer());

        return ActionResult.SUCCESS;
    }

    private void checkToolBroken(ItemStack sickle, PlayerEntity player) {
        if (sickle.getDamage() > sickle.getMaxDamage()) {
            // durability used up
            player.getEntityWorld().playSoundFromEntity( //
                    player, //
                    player, //
                    SoundEvents.ENTITY_ITEM_BREAK, //
                    SoundCategory.PLAYERS, //
                    1, //
                    1 //
            );
            sickle.decrement(1);
        }
    }

    private void harvestIfPlant(World world, BlockPos playerPos, BlockPos plantPos, ItemStack sickle) {
        final BlockState blockState = world.getBlockState(plantPos);
        final Block block = blockState.getBlock();
        if (block instanceof AbstractResourcePlant) {
            handleResourcePlant((AbstractResourcePlant) block, world, blockState, playerPos, plantPos, sickle);
        } else if (block instanceof CropBlock) {
            handleCrops((CropBlock) block, world, blockState, playerPos, plantPos, sickle);
        } else if (block instanceof StemBlock || block instanceof AttachedStemBlock || block instanceof SaplingBlock) {
            // do not break pumpkin or melon stems or saplings
        } else if (block instanceof NetherWartBlock) {
            handleWarts((NetherWartBlock) block, world, blockState, playerPos, plantPos, sickle);
        } else if (block instanceof SweetBerryBushBlock) {
            handleSweetberries(world, blockState, playerPos, plantPos, sickle);
        } else if (block instanceof GourdBlock || block instanceof PlantBlock) {
            // handle pumpkins, melons and all other plants
            handlePlant(world, blockState, playerPos, plantPos, sickle);
        } else if (block instanceof SugarCaneBlock) {
            handleStackablePlant(SugarCaneBlock.class, Items.SUGAR_CANE, world, playerPos, plantPos, sickle);
        } else if (block instanceof CactusBlock) {
            handleStackablePlant(CactusBlock.class, Items.CACTUS, world, playerPos, plantPos, sickle);
        }
    }

    /**
     * Handle {@link AbstractResourcePlant}s.
     */
    private void handleResourcePlant(AbstractResourcePlant resourcePlant, World world, BlockState blockState, BlockPos playerPos, BlockPos plantPos, ItemStack sickle) {
        if (resourcePlant.isMature(blockState)) {
            final List<ItemStack> stacks = resourcePlant.harvestAndReplant(blockState, world, plantPos);
            drop(world, playerPos, plantPos, stacks, sickle);
        }
    }

    /**
     * Handle farmable plants.
     */
    private void handleCrops(CropBlock cropBlock, World world, BlockState blockState, BlockPos playerPos, BlockPos plantPos, ItemStack sickle) {
        if (cropBlock.isMature(blockState)) {
            final List<ItemStack> stacks = Block.getDroppedStacks(blockState, (ServerWorld) world, plantPos, null);
            guessAndAdjustSeed(stacks);
            world.setBlockState(plantPos, cropBlock.getDefaultState());
            drop(world, playerPos, plantPos, stacks, sickle);
        }
    }

    /**
     * Nether warts are a special case.
     */
    private void handleWarts(NetherWartBlock wartBlock, World world, BlockState blockState, BlockPos playerPos, BlockPos plantPos, ItemStack sickle) {
        // warts do not have a public age getter, or a public maxAge getter
        if ((int) blockState.get(NetherWartBlock.AGE) >= 3) {
            final List<ItemStack> stacks = Block.getDroppedStacks(blockState, (ServerWorld) world, plantPos, null);
            guessAndAdjustSeed(stacks);
            world.setBlockState(plantPos, wartBlock.getDefaultState());
            drop(world, playerPos, plantPos, stacks, sickle);
        }
    }

    private void handleSweetberries(World world, BlockState blockState, BlockPos playerPos, BlockPos plantPos, ItemStack sickle) {
        // berry bushes do not have a public age getter, or a public maxAge getter
        if ((int) blockState.get(SweetBerryBushBlock.AGE) >= 3) {
            final List<ItemStack> stacks = Block.getDroppedStacks(blockState, (ServerWorld) world, plantPos, null);
            world.setBlockState(plantPos, blockState.with(SweetBerryBushBlock.AGE, 1), 2);
            drop(world, playerPos, plantPos, stacks, sickle);
        }
    }

    /**
     * Handle plants that do not grow anything, e.g. grass, scrubs.
     */
    private void handlePlant(World world, BlockState blockState, BlockPos playerPos, BlockPos plantPos, ItemStack sickle) {
        // just break the block
        final List<ItemStack> stacks = Block.getDroppedStacks(blockState, (ServerWorld) world, plantPos, null);
        world.setBlockState(plantPos, Charms.SOLID_AIR.getDefaultState());
        drop(world, playerPos, plantPos, stacks, sickle);
    }

    /**
     * Handle plants that grow in height: Sugar cane and cacti.
     */
    private <P extends Block> void handleStackablePlant(Class<P> plantClass, Item droppedItem, World world, BlockPos playerPos, BlockPos plantPos, ItemStack sickle) {
        // find bottom plant block
        BlockPos bottom = plantPos;
        while (plantClass.isInstance(world.getBlockState(bottom.down()).getBlock())) {
            bottom = bottom.down();
        }
        // fint top plant block
        BlockPos top = plantPos;
        while (plantClass.isInstance(world.getBlockState(top.up()).getBlock())) {
            top = top.up();
        }

        // height
        int height = top.getY() - bottom.getY();
        if (height == 0) {
            return;
        }

        // harvest
        for (int y = top.getY(); y > bottom.getY(); y--) {
            world.setBlockState(top, Charms.SOLID_AIR.getDefaultState());
            top = top.down();
        }
        // drop
        drop(world, playerPos, plantPos, Arrays.asList(new ItemStack(droppedItem, height)), sickle);
    }

    /**
     * Drop the items at the feet of the player, damage the sickle and play the crop-breaking sound.
     */
    private void drop(World world, BlockPos playerPos, BlockPos plantPos, List<ItemStack> stacks, ItemStack sickle) {
        if (stacks != null || stacks.isEmpty()) {
            stacks.forEach((itemStack) -> {
                Block.dropStack(world, playerPos, itemStack);
            });
            sickle.setDamage(sickle.getDamage() + 1);
        }
        world.playSound(null, plantPos, SoundEvents.BLOCK_CROP_BREAK, SoundCategory.BLOCKS, 0.8F,
                1.0F);
    }

    /**
     * For plants that drop their fruit AND their seed separately, try to guess what the seed is and lower that stack by 1.
     */
    private void guessAndAdjustSeed(List<ItemStack> stacks) {
        if (stacks == null || stacks.isEmpty()) {
            return;
        }

        for (ItemStack stack : stacks) {
            final Item item = stack.getItem();
            if (item == Items.WHEAT || item == Items.BEETROOT || item == Items.POISONOUS_POTATO) {
                // items that do not serve as seeds
                continue;
            } else {
                stack.decrement(1);
                break;
            }
        }
    }

}
