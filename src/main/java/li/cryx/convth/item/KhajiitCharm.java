package li.cryx.convth.item;

import li.cryx.convth.charm.AbstractCharm;
import li.cryx.convth.charm.CharmManager;
import li.cryx.convth.charm.CharmTickable;
import li.cryx.convth.util.TextUtil;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

import java.util.List;

public class KhajiitCharm extends AbstractCharm implements CharmTickable {

    private static final int DURATION = CharmManager.UPDATE_INTERVAL + 20;

    private int duration = DURATION;

    @Override
    public void doWork(ServerPlayerEntity player, ItemStack itemStack) {
        player.addStatusEffect(new StatusEffectInstance(
                StatusEffects.NIGHT_VISION,
                duration, // game ticks
                0, // amplifier (level-1)
                false,  // ambient (?)
                false, // show particles
                false // show icon
        ));
    }

    @Override
    public void appendTooltip(final ItemStack stack, final World world, final List<Text> tooltip,
                              final TooltipContext context) {
        tooltip.add(TextUtil.translate(Formatting.DARK_GRAY, getTranslationKey() + ".tooltip"));
    }

    public void setDuration(int gameTicks) {
        duration = gameTicks;
    }

    public void setBlinkingEnabled(boolean enabled) {
        if (!enabled) {
            setDuration(20 + 10 * 20 + CharmManager.UPDATE_INTERVAL);
        }
    }

}
