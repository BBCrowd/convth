package li.cryx.convth.item;

import li.cryx.convth.ConvenientThingsMod;
import li.cryx.convth.block.AbstractResourcePlant;
import li.cryx.convth.feature.ResourcePlants;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.Util;
import net.minecraft.util.registry.Registry;

/**
 * Seed item for a resource plant.
 *
 * <p>
 * Note: <code>BlockItem</code>s are <code>Item</code>s that represent broken
 * <code>Block</code>s. <code>BlockItem</code>s usually have the same name as
 * their <code>Block</code>.
 * </p>
 *
 * @author cryxli
 * @see ResourcePlants
 */
public class AbstractResourceSeed extends BlockItem {

    /**
     * Name the seeds differently than the corresponding block.
     */
    private String translationKey;

    /**
     * Create a seed for the given plant.
     *
     * @param plant The <code>Block</code> representing the plant.
     */
    public AbstractResourceSeed(final AbstractResourcePlant plant) {
        super(plant, new Item.Settings().group(ConvenientThingsMod.ITEM_GROUP));
    }

    @Override
    public String getTranslationKey() {
        // Have this BlockItem have a different name than the Block.
        if (translationKey == null) {
            translationKey = Util.createTranslationKey("item", Registry.ITEM.getId(this));
        }
        return translationKey;
    }

}
