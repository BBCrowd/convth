package li.cryx.convth.item;

import li.cryx.convth.charm.AbstractCharm;
import li.cryx.convth.charm.CharmManager;
import li.cryx.convth.charm.CharmTickable;
import li.cryx.convth.feature.Charms;
import li.cryx.convth.util.TextUtil;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.world.World;

import java.util.List;

/**
 * A repair charm does pick a damaged item and repairs a damage point when
 * activated by the {@link CharmManager}.
 *
 * @author cryxli
 * @see Charms
 */
public class RepairCharm extends AbstractCharm implements CharmTickable {

    @Override
    public void appendTooltip(final ItemStack stack, final World world, final List<Text> tooltip,
                              final TooltipContext context) {
        tooltip.add(TextUtil.translate(Formatting.DARK_GRAY, getTranslationKey() + ".tooltip1"));
        tooltip.add(TextUtil.translate(Formatting.DARK_GRAY, getTranslationKey() + ".tooltip2"));
    }

    @Override
    public void doWork(final ServerPlayerEntity player, final ItemStack itemStack) {
        // look for the next damaged item
        for (int index = 0; index < player.inventory.getInvSize(); index++) {
            final ItemStack stack = player.inventory.getInvStack(index);
            // items that do not support the vanilla "damage" mechanism are not inspected by
            // the charm
            if (stack.isDamaged()) {
                // repair one point of damage
                stack.setDamage(stack.getDamage() - 1);
                // only repair one item at a time
                break;
            }
        }
    }

}
