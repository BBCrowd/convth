package li.cryx.convth.item;

import li.cryx.convth.block.CharmSolidAirBlock;
import li.cryx.convth.charm.AbstractCharm;
import li.cryx.convth.charm.CharmFullTickable;
import li.cryx.convth.charm.CharmManager;
import li.cryx.convth.feature.Charms;
import li.cryx.convth.util.TextUtil;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.List;

/**
 * The flying charm imitates flying by placing {@link CharmSolidAirBlock}s under
 * players when they stand in mid air. The {@link CharmSolidAirBlock}s will
 * remove themselves from the world, if no player is near.
 *
 * @author cryxli
 * @see Charms
 */
public class FlyingCharm extends AbstractCharm implements CharmFullTickable {

    @Override
    public void appendTooltip(final ItemStack stack, final World world, final List<Text> tooltip,
                              final TooltipContext context) {
        tooltip.add(TextUtil.translate(Formatting.DARK_GRAY, "item.convth.charm_flying.tooltip1"));
    }

    /**
     * Place a block of {@link CharmSolidAirBlock}.
     *
     * @param world Current world object.
     * @param pos   Block position in the world.
     */
    private void placeSolidAir(final World world, final BlockPos pos) {
        final BlockState state = world.getBlockState(pos);
        // only place solid air, if player is "standing" on air
        if (state != null && state.isAir()) {
            world.setBlockState(pos, Charms.SOLID_AIR.getDefaultState());
        }
    }

    @Override
    public void tick(final ServerPlayerEntity player, final ItemStack itemStack) {
        final CompoundTag tag = CharmManager.checkTag(itemStack);
        int updateTick = tag.getInt(CharmManager.TAG_UPDATE_TICK) + 1;
        if (updateTick >= 2) {
            updateTick = 0;
            final World world = player.getEntityWorld();
            final BlockPos pos = player.getBlockPos().down();
            placeSolidAir(world, pos);
            placeSolidAir(world, pos.north());
            placeSolidAir(world, pos.south());
            placeSolidAir(world, pos.west());
            placeSolidAir(world, pos.east());
        }
        tag.putInt(CharmManager.TAG_UPDATE_TICK, updateTick);
    }

}
