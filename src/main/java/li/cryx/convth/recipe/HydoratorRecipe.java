package li.cryx.convth.recipe;

import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.InvalidIdentifierException;
import net.minecraft.util.registry.Registry;

import java.util.Objects;

/**
 * A single recipe for the {@link li.cryx.convth.block.HydoratorBlock}.
 *
 * <p><code>hashCode()</code> and <code>equals()</code> are build in such a way, the only block below
 * and input stack matter. This way a set can eliminate recipes with the same inputs.</p>
 *
 * @author cryxli
 */
public class HydoratorRecipe {

    /**
     * Name of the block below
     */
    private final Identifier blockBelow;

    /**
     * The input item.
     */
    private final ItemStack inputStack;

    /**
     * How many game ticks it takes to process the item.
     */
    private final int time;

    /**
     * The resulting item.
     */
    private final ItemStack outputStack;

    /**
     * Build a recipe from strings. Used to load config.
     *
     * @param blockBelow  Name of block below the hydorator.
     * @param inputStack  Name of the input stack.
     * @param time        Number of game ticks to process the item.
     * @param outputStack Name of the output stack.
     * @throws InvalidIdentifierException If any given item name cannot be parsed.
     */
    public HydoratorRecipe(String blockBelow, String inputStack, int time, String outputStack) {
        this(
                new Identifier(blockBelow),
                new ItemStack(Registry.ITEM.get(new Identifier(inputStack))),
                time,
                new ItemStack(Registry.ITEM.get(new Identifier(outputStack)))
        );
    }

    public HydoratorRecipe(Identifier blockBelow, ItemStack inputStack, int time, ItemStack outputStack) {
        this.blockBelow = blockBelow;
        this.inputStack = inputStack;
        this.time = time;
        this.outputStack = outputStack;
    }

    public int getTime() {
        return time;
    }

    public ItemStack getOutputStack() {
        return outputStack;
    }

    public ItemStack getInputStack() {
        return inputStack;
    }

    public Identifier getBlockBelow() {
        return blockBelow;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HydoratorRecipe that = (HydoratorRecipe) o;
        return Objects.equals(blockBelow, that.blockBelow) &&
                Objects.equals(inputStack, that.inputStack);
    }

    @Override
    public int hashCode() {
        return Objects.hash(blockBelow, inputStack);
    }

    @Override
    public String toString() {
        return "HydoratorRecipe{" +
                "blockBelow=" + blockBelow +
                ", inputStack=" + inputStack +
                ", time=" + time +
                ", outputStack=" + outputStack +
                '}';
    }

}
