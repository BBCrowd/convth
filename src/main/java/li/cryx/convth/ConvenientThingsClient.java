package li.cryx.convth;

import li.cryx.convth.feature.HydoratorFeature;
import li.cryx.convth.feature.ResourcePlants;
import li.cryx.convth.util.ModConfig;
import net.fabricmc.api.ClientModInitializer;

/**
 * Client related configs for the mod. Usually this only includes instructions
 * on how to render blocks.
 *
 * @author cryxli
 */
public class ConvenientThingsClient implements ClientModInitializer {

	@Override
	public void onInitializeClient() {
		final ModConfig conf = new ModConfig().load();
		new ResourcePlants(conf).onInitClient();
		new HydoratorFeature(conf).onInitClient();
	}

}
